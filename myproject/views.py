import mimetypes

from django.http import HttpResponse
import json
import pdfkit


def convert(request):
    if request.method == "GET":
        # body_unicode = request.body.decode('utf-8')
        # body = json.loads(body_unicode)
        html = "html"
        file_name = 'test.pdf'
        pdf = pdfkit.from_string(html,"myproject/" +file_name)
        import os
        file_full_path = os.path.dirname(os.path.abspath(__file__)) + "/test.pdf"
        # file_full_path = "/Users/dishantmakwana/myproject/test.pdf"
        with open(file_full_path,'r') as f:
            data = f.read()
        print len(data)
        response = HttpResponse(data, content_type=mimetypes.guess_type(file_full_path)[0])
        response['Content-Disposition'] = "attachment; filename={0}".format(file_name)
        response['Content-Length'] = os.path.getsize(file_full_path)
        return response